# Simple Docker Setup for WordPress development

## Installation

### Basic container setup

Copy .env.dist to .env and customize the ports as needed.
After issuing `docker-compose build` start up the containers with`docker-compose up -d`.
Now you should have a `db` and a `webserver` container up and running.

This will do the WordPress setup for you.
Now you can open `http://localhost` in your browser.

You can stop the services with `docker-compose down`.

### Setup WordPress

1. Copy `config/kickoff.sh` into the `html` directory on the host.
1. Log into run a shell in the `webserver` container.
1. Make the shell script executable (if it not already is) by `chmod a+x kickoff.sh`
1. Start the automatic setup process with `./kickoff.sh`
1. Log out of the container by pressing Ctrl-D

This installs WordPress with the following admin settings:
- Username: `admin`
- Password: `adminpw`
- EMail address: `test@test.com`
If this installation is reachable from the net, please change those settings immediately.

It uninstalls the akismet and hello plugins and installs the plugins WordPress Importer and All in One WordPress Migration.
It also does an updat of the core, all plugins, themes and translations.

## Usage

### Inspect log files

To attach to the log output of the `webserver` service, connect via `docker-compose logs -f webserver`. To detach just press Ctrl-C.

To shut it down open an second terminal and use `docker-compose down` to tear down the cluster.
To also delete all local images use `docker-compose down --rmi local`. After that you need to rebuild the images.

The html directory is a bound volume to /var/www/html in the web server image.

## Accessing the cluster nodes

- Webserver with PHP is under `localhost`
- MySQL runs under `localhost:3306`

## Internals
